import React, { Component } from 'react'
import {BrowserRouter,Redirect,Route,Switch} from 'react-router-dom'
import Login from './components/Login'
import Profile from './components/Profile'
import Translation from './components/Translation'


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <main className="App">
          <Switch>
            
            <Route path='/login' component={Login} />
            
            <Route path='/translate' component={Translation} />
            
            <Route path='/profile' component={Profile} />

            <Route path="/">
                <Redirect to="/login" />
            </Route>

          </Switch>
        </main>
      </BrowserRouter>
    )
  }
}

export default App
