import React, { Component } from 'react'
import Signs from '../Assets/index'

class Translation extends Component {

    state = {
        textToTranslate : '',
        signArray : []
    }
    
    // Keeps track off the input from the translate field. Will trigger on change
    handleTranslateChange(e) {
        this.setState({
            textToTranslate: e.target.value
        })
    }

    // Saves the translation 
    saveTranslate(){

        // Getting the translations from localStorage
        let translateArray = localStorage.getItem('translate')
        // Getting the current string to translate
        let translateSentence = this.state.textToTranslate;

        // If the local storage has not been initialized 
        if(translateArray == undefined){

            localStorage.setItem('translate',translateSentence)

        }
        // If the local storage has been initialized
        else{

            // Will split the String from local storage into an array
            translateArray = translateArray.split(',')
            
            // If the array is 10 the last element will be popped
            if(translateArray.length >= 10){
                translateArray.pop()
            }   

            // Puts in the newest translation in the front
            translateArray.unshift(translateSentence)

            // Stores the Array in local storage as a string where each element is comma separated 
            localStorage.setItem('translate',translateArray)
        }
        
    }

    // Translates the user input into sign language
    translate(){

        // Splits the array into characters and whitespaces
        let splitArray = this.state.textToTranslate.split('')
        let returnArray = []

        // Runs through the input array
        for(let i = 0; i < splitArray.length; i++){
            // Sets character to lowercase
            let character = splitArray[i].toLowerCase();

            // If the character is a white space, the character will be assigned to the string space
            // The string space translates to a whitespace picture
            if(character == " "){
                character = 'space';
            }

            // Puts the address of the sign into the return array
            returnArray.push(Signs[character])
        }

        // Saves the translate
        this.saveTranslate();

        // Sets the current singarray to the return array
        this.setState({
            signArray: returnArray
        })

        
    }
    
    // Route to profile
    routeToProfile(){
        this.props.history.push('/profile')
    }

    render() {
        return (
            <div style={container}>
                <div style={cardOuterBody}>
            
                    <div>
                        <h2>
                            Lost In Translation 
                        </h2>
                    </div>
                
                    <div style={flexInner}>
                        <input style={{...cardInnerSmall,...cardInnerBody}} type="text" placeholder="Word to translate" onChange={this.handleTranslateChange.bind(this)} />
                        <button style={{...buttonBlue,...btn}} onClick={this.routeToProfile.bind(this)}>Profile</button>
                        <button style={{...buttonBlue,...btn}} onClick={this.translate.bind(this)}>Translate</button>
                    </div>

                </div>

                <div style={cardOuterBody}>
                    <div style={{...cardInnerBig,...cardInnerBody}}>
                        {this.state.signArray.map(sign => {
                            return <img src={sign} style={picture} />
                        })}
                    </div>
                </div>

            </div>
        )
    }
}

const container = {
    display: 'flex',
    flexDirection: 'column',
    alignItems : 'center',
    height: '100vh',
    background: 'linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB)'
}

const cardOuterBody = {
    padding: '0.75rem 1.25rem',
    backgroundColor: '#eceff1',
    border: '1px solid #eceff1',
    width: '30vw',
    height: '35vh',
    borderRadius: '.25rem',
    display: 'flex',
    flexDirection: 'column',
    alignItems : 'center',
    justifyContent : 'center',
    marginTop: '30px'
}

const flexInner = {
    flexDirection: 'column',
    display: 'flex'
}

const cardInnerBody = {
    backgroundColor: '#fff',
    backgroundClip: 'border-box',
    border: '1px solid rgba(0,0,0,.125)',
    borderRadius: '.25rem',
    paddingLeft : '10px',
    margin : '5px',
    outline: 'none',
    overflow: 'auto'
}

const cardInnerSmall = {
    width: '26vw',
    height : '7vh',
}

const cardInnerBig = {
    width: '25vw',
    height : '30vh',
}

const buttonBlue = {
    background : 'linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB)',
    borderColor: '#007bff',
    width : '27vw',
    height : '7vh'
}

const btn = {
    display: 'inline-block',
    fontWeight: '400',
    textAlign: 'center',
    whiteSpace: 'nowrap',
    verticalAlign: 'middle',
    msUserSelect: 'none',
    userSelect: 'none',
    border: '1px solid transparent',
    margin : '5px',
    fontSize: '1rem',
    lineHeight: '1.5',
    borderRadius: '.25rem',
    transition: 'color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out',
    color: '#fff'
}

const picture = {
    width: '40px'
}

export default Translation
