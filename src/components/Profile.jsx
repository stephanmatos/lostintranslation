import React, { Component } from 'react'

class Profile extends Component {

    state = {
        translateArray : []
    }


    // Get a maximum of the 10 last translations
    getLastTenTranslates(){
        // Get the translate string
        let translateArray = localStorage.getItem('translate');
        // If null, no history of translates exist
        if(translateArray != null){
            // Split the string into an array
            translateArray = translateArray.split(',');

            // Set the array to the state array
            this.setState({
            translateArray : translateArray
            })    
        }
        
    }

    componentDidMount() {
        this.getLastTenTranslates();
    
    }

    // Clears local storage and returns to login screen
    logOut(){
        localStorage.clear();
        this.props.history.push('/login')
    }   

    // Returns to translate screen
    routeToTranslate(){
        this.props.history.goBack();
    }

    render() {
        return (
            <div style={container}>
                <div style={cardOuterBody}>
            
                    <div>
                        <h2>
                            Profile
                        </h2>
                        <h2>
                            {localStorage.getItem('name')}
                        </h2>
                    </div>
                
                    <div style={flexInner}>
                    <button style={{...buttonBlue,...btn}} onClick={this.routeToTranslate.bind(this)} >Translate</button>
                        <button style={{...buttonBlue,...btn}} onClick={this.logOut.bind(this)} >Log Out</button>
                    </div>
                    
                </div>

                <div style={cardOuterBody}>
                    <div style={{...cardInnerBig,...cardInnerBody}}>
                         {this.state.translateArray.map((sentence,index) => {
                            return <p key={index}>{sentence}</p>
                        })}
                    </div>
                </div>

            </div>
        )
    }
}

const container = {
    display: 'flex',
    flexDirection: 'column',
    alignItems : 'center',
    height: '100vh',
    background: 'linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB)'
}

const cardOuterBody = {
    padding: '0.75rem 1.25rem',
    backgroundColor: '#eceff1',
    border: '1px solid #eceff1',
    width: '30vw',
    height: '35vh',
    borderRadius: '.25rem',
    display: 'flex',
    flexDirection: 'column',
    alignItems : 'center',
    justifyContent : 'center',
    marginTop: '30px'
}

const flexInner = {
    flexDirection: 'column',
    display: 'flex'
}

const cardInnerBody = {
    backgroundColor: '#fff',
    backgroundClip: 'border-box',
    border: '1px solid rgba(0,0,0,.125)',
    borderRadius: '.25rem',
    paddingLeft : '10px',
    margin : '5px',
    outline: 'none',
    overflow: 'auto'
}

const cardInnerSmall = {
    width: '26vw',
    height : '7vh',
}

const cardInnerBig = {
    width: '25vw',
    height : '30vh',
}

const buttonBlue = {
    background : 'linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB)',
    borderColor: '#007bff',
    width : '27vw',
    height : '7vh'
}

const btn = {
    display: 'inline-block',
    fontWeight: '400',
    textAlign: 'center',
    whiteSpace: 'nowrap',
    verticalAlign: 'middle',
    msUserSelect: 'none',
    userSelect: 'none',
    border: '1px solid transparent',
    margin : '5px',
    fontSize: '1rem',
    lineHeight: '1.5',
    borderRadius: '.25rem',
    transition: 'color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out',
    color: '#fff'
}

export default Profile
