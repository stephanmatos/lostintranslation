import React, { Component } from 'react'

class Login extends Component {

    state = {
        name : ''
    }
    
    // tracks the input field for name
    handleNameChange(e) {
        this.setState({
            name: e.target.value
        })
    }

    // Logs the user in and stores the user name in local storage
    handleLogin(){
        if(this.state.name.length >= 2){
            localStorage.setItem('name',this.state.name)
            this.props.history.push('/translate')
        }else{
            window.alert("You must enter at least two characters");

        }
        
    }


    render() {

        return (
            <div style={container}>

                <div style={cardOuterBody}>
                    
                    <h2>
                        Lost In Translation 
                    </h2>
            
                    <input style={cardInnerBody} type="text" placeholder="Name" onChange={this.handleNameChange.bind(this)} />

                    <button style={{...buttonBlue,...btn}} onClick={this.handleLogin.bind(this)} >Login</button>
            
                </div>
               
            </div>
        )


    }

    

    
}



const container = {
    display: 'flex',
    flexDirection: 'column',
    alignItems : 'center',
    height: '100vh',
    justifyContent : 'center',
    background: 'linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB)'
}

const cardOuterBody = {
    padding: '0.75rem 1.25rem',
    backgroundColor: '#eceff1',
    border: '1px solid #eceff1',
    width: '30vw',
    height: '50vh',
    borderRadius: '.25rem',
    display: 'flex',
    flexDirection: 'column',
    alignItems : 'center',
    justifyContent : 'center'
}

const cardInnerBody = {
    backgroundColor: '#fff',
    backgroundClip: 'border-box',
    border: '1px solid rgba(0,0,0,.125)',
    borderRadius: '.25rem',
    paddingLeft : '10px',
    width: '26vw',
    height : '7vh',
    margin : '10px',
    outline: 'none'
}


const buttonBlue = {
    background : 'linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB)',
    borderColor: '#007bff',
    width : '27vw',
    height : '7vh'
}

const btn = {
    display: 'inline-block',
    fontWeight: '400',
    textAlign: 'center',
    whiteSpace: 'nowrap',
    verticalAlign: 'middle',
    msUserSelect: 'none',
    userSelect: 'none',
    border: '1px solid transparent',
    margin : '10px',
    fontSize: '1rem',
    lineHeight: '1.5',
    borderRadius: '.25rem',
    transition: 'color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out',
    color: '#fff'
}

export default Login